<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="PIXINVENT">
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/vendors.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/charts/apexcharts.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/tether-theme-arrows.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/tether.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/shepherd-theme-default.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/datatables.min.css">

    <link href="app-assets/css/plugins/gijgo/gijgo.min.css" rel="stylesheet" type="text/css" />
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/components.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/dark-layout.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/themes/semi-dark-layout.css">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/dashboard-analytics.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/dashboard-ecommerce.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/card-analytics.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/tour/tour.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/plugins/forms/wizard.css">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END: Custom CSS-->
    <style>
        .form-control { border-radius: 20px !important; }
        body {
            background-color: #ffffff !important;
        }
        body.vertical-layout.vertical-menu-modern.menu-collapsed .main-menu:not(.expanded) .navigation li.active a {
            /* background: linear-gradient(
            118deg
            , #f0a667, rgb(240 192 103 / 70%)); */
            background-color: #ffa50059;
            /* box-shadow: 0 0 10px 1px rgb(0 0 0 / 34%); */
            color: #fff;
            font-weight: 400;
            border-radius: 4px;
        }
        @media only screen and (max-width: 747px) {
            .cari-dok {
                /* display: none !important; */
                width: 222px !important;
            }
        }
        @media only screen and (max-width: 420px) {
            .cari-dok {
                /* display: none !important; */
                width: 182px !important;
            }
        }
        @media only screen and (max-width: 386px) {
            .cari-dok {
                /* display: none !important; */
                width: 150px !important;
            }
        }
        @media only screen and (max-width: 355px) {
            .cari-dok {
                /* display: none !important; */
                width: 120px !important;
            }
        }
        @media only screen and (max-width: 335px) {
            .cari-dok {
                /* display: none !important; */
                width: 99px !important;
            }
        }
        @media only screen and (max-width: 518px) {
            .br {
                display: block !important;
            }
        }
        @media only screen and (max-width: 1200px) {
            #back-arrow {
                /* display: block !important; */
                top: 12px;
                left: 50px;
            }
        }

        @media only screen and (max-width: 822px) {
            #back-arrow {
                display: none !important;
            }
        }
    </style>

    <!-- BEGIN: Vendor JS-->
    <script src="app-assets/vendors/js/vendors.min.js"></script>
    <!-- BEGIN Vendor JS-->