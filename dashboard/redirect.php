<?php
$page = (isset($_GET['page']))? $_GET['page'] : '';

switch($page){
  case 'dashboard': 
  include "content/dashboard.php"; 
  break;

  case 'list': 
  include "content/list.php"; 
  break;

  case 'list-add': 
  include "content/list-add.php"; 
  break;
  
  default: 
  include "content/dashboard.php";
}
?>