        <style>
            #nav-user { 
                border-collapse: separate; 
                border-spacing: 0;
                background-color: white;
                border-top-left-radius: 25px;
                border-top-right-radius: 25px;
                border-bottom-left-radius: 25px;
                border-bottom-right-radius: 25px;
            }
            #nav-user tr td { border: 1px solid #c5c5c582 }
            #nav-user tr:first-child td:first-child {
                border-top-left-radius: 25px;
                font-size: 23px;
                box-shadow: rgb(0 0 0 / 15%) -1px 2px 4px;
            }
            #nav-user tr:first-child td:last-child { 
                border-top-right-radius: 25px; 
                box-shadow: rgb(0 0 0 / 15%) -1px 2px 4px;
            }
            #nav-user tr:last-child td:first-child { border-bottom-left-radius: 25px; }
            #nav-user tr:last-child td:last-child { border-bottom-right-radius: 25px; }
            .list-notif {
                white-space: unset;
                word-break: break-word;
            }
            .cari-dok { width: 390px; }
            #badge-custom { 
                position: absolute; 
                top: 3px; 
                right: 15%; 
                font-size: 35%;
            }
        </style>
        
        <div class="navbar-wrapper">
            <div class="navbar-container content" style="padding-left: 0rem !important;">
                <div class="navbar-collapse" id="navbar-mobile" style="display: initial !important;">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto">
                                <a class="nav-link nav-menu-main menu-toggle" style="padding: 12px 0 0 0 !important;" href="#">
                                    <i class="ficon feather icon-menu"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php 
                    $page = (isset($_GET['page']))? $_GET['page'] : null;
                    if ($page == 'list' || $page == 'list-add') { ?>
                    <ul class="nav navbar-nav" id="back-arrow" style="position: absolute; display: block;">
                        <li class="nav-item d-lg-block">
                            <a href="javascript:history.back()" style="font-size: 15px; color: #626262">
                                <b>
                                <i class="feather icon-arrow-left" style="font-weight: bolder;"></i>
                                Back
                                </b>
                            </a>
                        </li>
                    </ul>
                    <?php }?>
                    <ul class="nav navbar-nav float-right">
                        <li class="nav-item d-lg-block" style="margin: 4px 8px 0px 0px;">
                            <div class="cari-dok">
                                <fieldset class="form-label-group form-group position-relative" >
                                    <input type="text" class="form-control" id="iconLabelRight" placeholder="Cari Dokumen">
                                    <div class="form-control-position">
                                        <a href="javascript:void(0)"><i class="feather icon-search"></i></a>
                                    </div>
                                    <label for="iconLabelRight">Cari Dokumen</label>
                                </fieldset>
                            </div>
                        </li>
                        <li class="dropdown dropdown-user nav-item">
                            <table id="nav-user">
                                <tr>
                                    <td>
                                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown" style="padding: 0 0 0 12px; color: black">
                                            <div class="position-relative d-inline-block">
                                                <i class="feather icon-bell" ></i>
                                                <span class="badge badge-pill badge-danger badge-up badge-custom" id="badge-custom">3</span>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" style="left:auto;right:auto">
                                            <a class="dropdown-item list-notif" href="javascript:void(0)">Notif 1</a>
                                            <a class="dropdown-item list-notif" href="javascript:void(0)">Notif 2</a>
                                            <a class="dropdown-item list-notif" href="javascript:void(0)">Notif 3</a>
                                        </div>
                                    </td>
                                    <td>
                                        <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown" style="padding: 0 0 0 20px; color: black">
                                            <div class="user-nav d-sm-flex d-none">
                                                <span class="user-name text-bold-600" style="margin-bottom: 0">MOHAMAD ADHI</span>
                                                <span class="user-status">Adhiluthfi</span>
                                            </div>
                                            <span><img class="round" src="app-assets/images/portrait/small/avatar-s-11.jpg" alt="avatar" height="40" width="40"></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item" href="auth"><i class="feather icon-power"></i> Logout</a>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
            </div>
        </div>