        <div class="navbar-header" style="height: 130px">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="dashboard">
                        <img src="assets/images/kemendagri.png" alt="" width="35">
                        <img src="assets/images/bsn-logo.png" alt="" width="53" style="position: fixed; top: 80px; left: 14px;">
                    </a>
                </li>
                <li class="nav-item nav-toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                        <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                        <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" style="background: rgb(76 175 80 / 0%);">
                <li >
                    <div class="badge badge-warning mr-1 mb-1" style="width: 50px; cursor: pointer;">
                        <i class="feather icon-upload-cloud" style="line-height: 1.5;font-size: 20px;"></i>
                    </div>
                </li>
                <li class="nav-item" style="margin-top: 20px;" id="dashboard">
                    <a  href="dashboard" style="color: gray">
                        <i class="feather icon-home"></i>
                        <span class="menu-title" data-i18n="Analytics">Home</span>
                    </a>
                </li>
                <li class="nav-item" id="list">
                    <a  href="dashboard?page=list" style="color: gray">
                        <i class="feather icon-list"></i>
                        <span class="menu-title" data-i18n="Analytics">List</span>
                    </a>
                </li>
            </ul>
        </div>