    
                    <style>
                        #card-custom1 { 
                            background-image: linear-gradient(150deg, #4c8291, #10163a, #5a4886); 
                            position: relative; 
                        }
                        .card-custom2 {  
                            margin:30px 0 50px 0px; 
                            background-color: #e8e8e8;
                        }
                        .icon-card { font-size: 30px; color: orange; padding-bottom: 5px }
                        #tbl-card { border-collapse: collapse; }
                        #tbl-card tr { border: none; }
                        #tbl-card tr td { width: 33.3333%; vertical-align: top }
                        .td-border { border-right: solid 0.06rem #00000038; }
                        .text-icon { line-height: 1rem; padding-top: 10px }
                        .text-bottom-left {
                            color: white;
                            position: absolute;
                            bottom: 3%;
                        }
                        .text-bottom-right {
                            color: white;
                            position: absolute;
                            bottom: 3%;
                            right: 19px;
                        }
                        .badge-custom {
                            font-size: 15px; 
                            border: 1px solid #8080804a;
                        }
                        .actions { padding-top: 20px }
                        .icon-actions {
                            font-size: 20px;
                            padding-right: 7px;
                        }
                        .actions p a { color:#626262 }
                        .br {
                            display: none;
                        }
                    </style>

                    <h2><b>Selamat Datang Kembali,<br class="br"> MOHAMAD</b></h2>
                    <div class="badge badge-pill bg-white black badge-custom" >
                        <i class="feather icon-check-circle success"></i>  Verified
                    </div>
                    <div class="row" style="padding-top: 50px;">
                        <div class="col-lg-5 col-md-6 col-12">
                            <div class="card" id="card-custom1">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="row w-100">
                                        <div class="col-12">
                                            <h5 class="text-bold-700 white">
                                                Tingkat Penilaian Smartcity
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="row w-100">
                                        <div class="col-12 white">
                                        <p>Ada beberapa tahap penilaian dan tinjauan untuk
                                        memverifikasi data yang sudah anda ajukan.</p>
                                        </div>
                                    </div>
                                    <div class="card w-100 card-custom2">
                                        <div class="card-content text-center" style="margin: 7px">
                                            <div class="table-responsive mt-0">
                                                <table class="w-100" id="tbl-card">
                                                    <tr>
                                                        <td class="td-border">
                                                        <a href="javscript:void(0)">
                                                            <i class="feather icon-clock icon-card"></i>
                                                            <p class="black text-icon">Verifikasi Pengajuan</p>
                                                        </a>
                                                        </td>
                                                        <td class="td-border">
                                                        <a href="javscript:void(0)">
                                                            <i class="feather icon-edit icon-card"></i>
                                                            <p class="black text-icon">Melengkapi Form</p>
                                                        </a>
                                                        </td>
                                                        <td>
                                                            <a href="javscript:void(0)">
                                                                <i class="feather icon-rotate-ccw icon-card"></i>
                                                                <p class="black text-icon">History</p>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-bottom-left">
                                        Data terupdate
                                    </div>
                                    <div class="text-bottom-right">
                                        27 April 2021
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-12">
                            <div class="actions">
                                <p><a href="javascript:void(0)"><i class="feather icon-file-text icon-actions"></i> Lihat Semua Dokumen</a></p>
                                <p><a href="javascript:void(0)"><i class="feather icon-refresh-ccw icon-actions"></i> Ativitas</a></p>
                                <p><a href="javascript:void(0)"><i class="feather icon-plus-circle icon-actions"></i> Tambah Dokumen Pendukung</a></p>
                                <p><a href="javascript:void(0)"><i class="feather icon-help-circle icon-actions"></i> Butuh Bantuan?</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="text-dokumen-tertunda">
                        <b>
                            <span>Menunggu Respon Tim Penilai</span>
                            <span class="warning" style="float: right">Lihat semua dokumen tertunda <i class="fa fa-spin fa-spinner"></i></span>
                        </b>
                    </div>
                    <br class="br">
                    <hr style="border-top: 2px solid #22292f1a;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card" style="background-color: #e8e8e8; padding-top: 6px;">
                                <div class="card-content">
                                    <img src="assets/images/digital-city.png" alt="">
                                    <span style="float:right; text-align:right; padding: 22px 28px 0 0;">
                                        <h1 style="font-size: 2.3rem;padding-bottom: 10px;">Semuanya Mudah!</h1>
                                        <p>Anda cukup mengajukan satu form untuk pengajuan penilaian<br>
                                        Lalu tim kami akan verifikasi pengajuan anda</p>
                                        <h1 style="font-size:3rem"><i class="fa fa-check-circle" style="color: #56ce0c"></i></h2>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- BEGIN: Page Vendor JS-->
                    <script src="app-assets/vendors/js/charts/apexcharts.js"></script>
                    <!-- END: Page Vendor JS-->

                    <!-- BEGIN: Page JS-->
                    <script src="app-assets/js/scripts/pages/dashboard-analytics.js"></script>
                    <script src="app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
                    <!-- END: Page JS-->
                