                    <style>
                        #card-info tr td {
                            padding: 3px;
                            vertical-align: top;
                        }
                        #kriteria th:first-child { width: 5% }
                        #kriteria th:nth-child(2) { width: 30% }
                        .bg-e8 { background-color: #e8e8e8; }
                        .tbl-input { border: hidden }
                        .tbl-input thead tr { border: hidden }
                        .tbl-input th { width: 33.3333% !important }
                        .tbl-input textarea { border: hidden; width:85% }
                        .tbl-input td { padding: 0; vertical-align: top }
                        
                    </style>
                    <h2><b>Mengisi Data Dokumen Lain</b></h2>
                    <div class="row" style="padding-top: 50px; padding-bottom: 25px;">
                        <div class="col-lg-5 col-md-6 col-12">
                            <div class="card" style="background-color: #e8e8e8">
                                <div class="card-header d-flex flex-column align-items-start" style="padding: 10px">
                                    <div class="row w-100">
                                        <div class="col-12">
                                            <div class="table-responsive mt-0">
                                            <table id="card-info" class="w-100">
                                                <tr>
                                                    <td>No. Registrasi</td>
                                                    <td>:</td>
                                                    <td>1234657894651320</td>
                                                </tr>
                                                <tr>
                                                    <td>Nama Pemerintah Daerah</td>
                                                    <td>:</td>
                                                    <td>Kota Jakarta Selatan</td>
                                                </tr>
                                                <tr>
                                                    <td>Kontak Person Pemohon</td>
                                                    <td>:</td>
                                                    <td>Adhi Luthfi</td>
                                                </tr>
                                                <tr>
                                                    <td>Nomor HP</td>
                                                    <td>:</td>
                                                    <td>081234657890</td>
                                                </tr>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <b>
                                <span style="float: left;padding-top: 14px;"><h4>Kriteria Penilaian</h4></span>
                            </b>
                            <a href="dashboard?page=list-add" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light pull-right">Add New</a>
                        </div>
                    </div>
                    <hr style="border-top: 2px solid #22292f1a;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-content">
                                    <div class="table-responsive mt-0">
                                        <table class="table" id="kriteria">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Alat Penilaian Kematangan kota Cerdas</th>
                                                    <th>Tanggal Update</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Layanan Pendidikan</td>
                                                    <td>Apr 22, 2021 14:30</td>
                                                    <td><div class="badge badge-pill badge-light-success mr-1 mb-1">Submitted</div></td>
                                                    <td><a href="javascript:void(0)"><u>Lihat Dokumen</u></a></td>
                                                    <td><a href="javascript:void(0)" class="tr-collapse-1"><i class="feather icon-chevron-down"></i></a></td>
                                                </tr>
                                                <tr class="collapse-1">
                                                    <td></td>
                                                    <td colspan="5">
                                                        <table class="text-center tbl-input w-100 table bg-e8">
                                                            <thead>
                                                                <tr>
                                                                    <th>Persyaratan Indikator</th>
                                                                    <th>Data Yang Dibutuhkan</th>
                                                                    <th>Dokumen Pendukung</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><textarea rows="4"></textarea></td>
                                                                    <td><textarea rows="4"></textarea></td>
                                                                    <td><input type="file" style="background-color: white"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="pull-right">
                                                            <button type="button" id="cancel-collapse-1" class="btn btn-outline-warning round mr-1 mb-1 waves-effect waves-light">CANCEL</button>
                                                            <button type="button" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light">SAVE</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2</td>
                                                    <td>Layanan Kesehatan</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><a href="javascript:void(0)"><u>Lihat Dokumen</u></a></td>
                                                    <td><a href="javascript:void(0)" class="tr-collapse-2"><i class="feather icon-chevron-down"></i></a></td>
                                                </tr>
                                                <tr class="collapse-2">
                                                    <td></td>
                                                    <td colspan="5">
                                                        <table class="text-center tbl-input w-100 table bg-e8">
                                                            <thead>
                                                                <tr>
                                                                    <th>Persyaratan Indikator</th>
                                                                    <th>Data Yang Dibutuhkan</th>
                                                                    <th>Dokumen Pendukung</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><input type="file" style="background-color: white"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="pull-right">
                                                            <button type="button" id="cancel-collapse-2" class="btn btn-outline-warning round mr-1 mb-1 waves-effect waves-light">CANCEL</button>
                                                            <button type="button" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light">SAVE</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Layanan Sosial</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><a href="javascript:void(0)"><u>Lihat Dokumen</u></a></td>
                                                    <td><a href="javascript:void(0)" class="tr-collapse-3"><i class="feather icon-chevron-down"></i></a></td>
                                                </tr>
                                                <tr class="collapse-3">
                                                    <td></td>
                                                    <td colspan="5">
                                                        <table class="text-center tbl-input w-100 table bg-e8">
                                                            <thead>
                                                                <tr>
                                                                    <th>Persyaratan Indikator</th>
                                                                    <th>Data Yang Dibutuhkan</th>
                                                                    <th>Dokumen Pendukung</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><input type="file" style="background-color: white"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="pull-right">
                                                            <button type="button" id="cancel-collapse-3" class="btn btn-outline-warning round mr-1 mb-1 waves-effect waves-light">CANCEL</button>
                                                            <button type="button" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light">SAVE</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Layanan Perumahan Rakyat dan Pemukiman</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><a href="javascript:void(0)"><u>Lihat Dokumen</u></a></td>
                                                    <td><a href="javascript:void(0)" class="tr-collapse-4"><i class="feather icon-chevron-down"></i></a></td>
                                                </tr>
                                                <tr class="collapse-4">
                                                    <td></td>
                                                    <td colspan="5">
                                                        <table class="text-center tbl-input w-100 table bg-e8">
                                                            <thead>
                                                                <tr>
                                                                    <th>Persyaratan Indikator</th>
                                                                    <th>Data Yang Dibutuhkan</th>
                                                                    <th>Dokumen Pendukung</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><input type="file" style="background-color: white"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="pull-right">
                                                            <button type="button" id="cancel-collapse-4" class="btn btn-outline-warning round mr-1 mb-1 waves-effect waves-light">CANCEL</button>
                                                            <button type="button" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light">SAVE</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>Layanan Pekerjaan Umum dan Penataan Ruang</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><a href="javascript:void(0)"><u>Lihat Dokumen</u></a></td>
                                                    <td><a href="javascript:void(0)" class="tr-collapse-5"><i class="feather icon-chevron-down"></i></a></td>
                                                </tr>
                                                <tr class="collapse-5">
                                                    <td></td>
                                                    <td colspan="5">
                                                        <table class="text-center tbl-input w-100 table bg-e8">
                                                            <thead>
                                                                <tr>
                                                                    <th>Persyaratan Indikator</th>
                                                                    <th>Data Yang Dibutuhkan</th>
                                                                    <th>Dokumen Pendukung</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><input type="file" style="background-color: white"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="pull-right">
                                                            <button type="button" id="cancel-collapse-5" class="btn btn-outline-warning round mr-1 mb-1 waves-effect waves-light">CANCEL</button>
                                                            <button type="button" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light">SAVE</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>6</td>
                                                    <td>Layanan Ketentraman, Ketertiban Umum dan Perlindungan Masyarakat</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><a href="javascript:void(0)"><u>Lihat Dokumen</u></a></td>
                                                    <td><a href="javascript:void(0)" class="tr-collapse-6"><i class="feather icon-chevron-down"></i></a></td>
                                                </tr>
                                                <tr class="collapse-6">
                                                    <td></td>
                                                    <td colspan="5">
                                                        <table class="text-center tbl-input w-100 table bg-e8">
                                                            <thead>
                                                                <tr>
                                                                    <th>Persyaratan Indikator</th>
                                                                    <th>Data Yang Dibutuhkan</th>
                                                                    <th>Dokumen Pendukung</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><textarea class="w-75" rows="4"></textarea></td>
                                                                    <td><input type="file" style="background-color: white"></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <div class="pull-right">
                                                            <button type="button" id="cancel-collapse-6" class="btn btn-outline-warning round mr-1 mb-1 waves-effect waves-light">CANCEL</button>
                                                            <button type="button" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light">SAVE</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- BEGIN: Page Vendor JS-->
                    <script src="app-assets/vendors/js/charts/apexcharts.js"></script>
                    <!-- END: Page Vendor JS-->

                    <!-- BEGIN: Page JS-->
                    <script src="app-assets/js/scripts/pages/dashboard-analytics.js"></script>
                    <script src="app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
                    <!-- END: Page JS-->
                    <script>
                        $(function () {
                            for (let i = 0; i <= 6; i++) {
                                $('.collapse-'+i).hide();
                                $('.tr-collapse-'+i+', #cancel-collapse-'+i).click(function (e) { 
                                    $('.collapse-'+i).toggle(300);
                                    $('.tr-collapse-'+i).children().toggleClass('icon-chevron-up')
                                    // return false;
                                });
                            }
                        });
                    </script>
                