                    <div class="row" style="padding-bottom: 25px;">
                       <div class="col-md-6 col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title"><b>Form Add New Kriteria</b></h3>
                                </div>
                                <br>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form form-horizontal">
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>Nama</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <input type="text" id="first-name" class="form-control" name="nama" placeholder="Nama">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>Persyaratan Indikator</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <textarea name="persyaratan_indikator" id="" cols="30" rows="5" class="form-control" placeholder="Persyaratan Indikator"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                                <span>Requirement</span>
                                                            </div>
                                                            <div class="col-md-8">
                                                                <textarea name="requirement" id="" cols="30" rows="5" class="form-control" placeholder="Requirement"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-4">
                                                            </div>
                                                            <div class="col-md-8">
                                                                <ul class="list-unstyled mb-0">
                                                                    <li class="d-inline-block mr-2">
                                                                        <fieldset>
                                                                            <div class="vs-radio-con vs-radio-warning">
                                                                                <input type="radio" name="vueradio" checked="" value="false">
                                                                                <span class="vs-radio">
                                                                                    <span class="vs-radio--border"></span>
                                                                                    <span class="vs-radio--circle"></span>
                                                                                </span>
                                                                                <span class="">Active</span>
                                                                            </div>
                                                                        </fieldset>
                                                                    </li>
                                                                    <li class="d-inline-block mr-2">
                                                                        <fieldset>
                                                                            <div class="vs-radio-con vs-radio-warning">
                                                                                <input type="radio" name="vueradio" value="false">
                                                                                <span class="vs-radio">
                                                                                    <span class="vs-radio--border"></span>
                                                                                    <span class="vs-radio--circle"></span>
                                                                                </span>
                                                                                <span class="">Inactive</span>
                                                                            </div>
                                                                        </fieldset>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-8 offset-md-4">
                                                        <button type="submit" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light">SUBMIT</button>
                                                        <button type="reset" id="cancel-collapse-1" class="btn btn-outline-warning round mr-1 mb-1 waves-effect waves-light">RESET</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- BEGIN: Page Vendor JS-->
                    <script src="app-assets/vendors/js/charts/apexcharts.js"></script>
                    <!-- END: Page Vendor JS-->

                    <!-- BEGIN: Page JS-->
                    <script src="app-assets/js/scripts/pages/dashboard-analytics.js"></script>
                    <script src="app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
                    <!-- END: Page JS-->
                    <script>
                        $(function () {
                            for (let i = 0; i <= 6; i++) {
                                $('.collapse-'+i).hide();
                                $('.tr-collapse-'+i+', #cancel-collapse-'+i).click(function (e) { 
                                    $('.collapse-'+i).toggle(300);
                                    $('.tr-collapse-'+i).children().toggleClass('icon-chevron-up')
                                    // return false;
                                });
                            }
                        });
                    </script>
                