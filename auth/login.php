                        <div class="card bg-authentication rounded-0 mb-0">
							<div class="row m-0">
								<!-- <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
									<img src="../app-assets/images/pages/login.png" alt="branding logo">
								</div> -->
								<div id="responsive" class="col-lg-12 col-12 p-0" 
                                    style="
                                        height: 620px; 
                                        width: 390px;"
                                    >
									<div class="card rounded-0 mb-0 px-2" style="height: 620px;" id="card-login">
										<!-- <div class="card-header pb-1">
											<div class="card-title">
												<h4 class="mb-0">Login</h4>
											</div>
										</div> -->
										<p class="px-2"></p>
										<div class="card-content">
											<div class="card-body pt-1">
												<table style="margin-bottom: 20px; width: 100%;">
													<tr>
														<td style="text-align: right;">
															<img src="../assets/images/kemendagri.png" alt="" width="70" style="padding-right: 10px;"> 
														</td>
														<td>
															<img src="../assets/images/bsn-logo.png" alt="" width="80">
														</td>
													</tr>
												</table	>
												<div style="text-align: center; padding-bottom: 20px;"><b style="color: orange;">Login</b><br>with your PemdaID Account</div>
												<form action="index.html">
													<fieldset
														class="form-label-group form-group position-relative has-icon-left">
														<input type="text" class="form-control" id="iconLabelRight" 
															placeholder="PemdaID" required>
														<div class="form-control-position">
															<i class="feather icon-user"></i>
														</div>
														<label for="user-name">PemdaID</label>
													</fieldset>
													<fieldset class="form-label-group position-relative has-icon-left">
														<input type="password" class="form-control" id="user-password"
															placeholder="Password" required>
														<div class="form-control-position">
															<a href="javascript:void(0)" onclick="visible()"><i class="feather icon-eye"></i></a>
														</div>
														<label for="user-password">Password</label>
													</fieldset>
                                                    <div class="text-center">
                                                        <a href="javascript:void(0)">Forgot your PemdaID or Password</a>
                                                    </div>
													<!-- <div
														class="form-group d-flex justify-content-between align-items-center"> -->
														<!-- <div class="text-left">
															<fieldset class="checkbox">
																<div class="vs-checkbox-con vs-checkbox-primary">
																	<input type="checkbox">
																	<span class="vs-checkbox">
																		<span class="vs-checkbox--check">
																			<i class="vs-icon feather icon-check"></i>
																		</span>
																	</span>
																	<span class="">Remember me</span>
																</div>
															</fieldset>
														</div> -->
													<!-- </div> -->
													<!-- <a href="auth-register.html"
														class="btn btn-outline-primary float-left btn-inline">Register</a> -->
													</form>
												</div>
											</div>
											<div class="login-footer">
												<div class="text-center">
													<button type="button" onclick="dashboard()" class="btn btn-warning round mr-1 mb-1 waves-effect waves-light">CONTINUE</button>
												</div>
												<hr style="border-top: 1px solid #bbb; padding-bottom: 13px;">
												<div class="text-center">
													<button type="button" onclick="register1()" class="btn btn-outline-warning round mr-1 mb-1 waves-effect waves-light">REGISTER ACCOUNT</button>
												</div>
											</div>
											<div class="text-center">
												Powered By
											</div>
											<table style="width: 100%;">
												<tr>
													<td class="text-right"><img src="../assets/images/google-logo.png" alt="" width="70" style="padding-right: 10px;"></td>
													<td><img src="../assets/images/elitery-logo.jpg" width="62" alt=""></td>
												</tr>
											</table>
											<!-- <div class="footer-btn d-inline">
												<a href="#" class="btn btn-facebook"><span
														class="fa fa-facebook"></span></a>
												<a href="#" class="btn btn-twitter white"><span
														class="fa fa-twitter"></span></a>
												<a href="#" class="btn btn-google"><span
														class="fa fa-google"></span></a>
												<a href="#" class="btn btn-github"><span
														class="fa fa-github-alt"></span></a>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>