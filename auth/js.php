<!-- BEGIN: Vendor JS-->
	<script src="../app-assets/vendors/js/vendors.min.js"></script>
	<!-- BEGIN Vendor JS-->

	<!-- BEGIN: Page Vendor JS-->
	<!-- END: Page Vendor JS-->

	<!-- BEGIN: Theme JS-->
	<script src="../app-assets/js/core/app-menu.js"></script>
	<script src="../app-assets/js/core/app.js"></script>
	<script src="../app-assets/js/scripts/components.js"></script>
	<script>
		function visible() {
			var x = document.getElementById("user-password");
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		}

		function dashboard() {
			window.location.href = "../dashboard";
		}

		function login() {
			window.location.href = "index.php";
		}

		function register1() {
			window.location.href = "?page=register1";
		}

		function register2() {
			window.location.href = "?page=register2";
		}

		function register3() {
			window.location.href = "?page=register3";
		}
	</script>
	<!-- END: Theme JS-->

	<!-- BEGIN: Page JS-->
	<!-- END: Page JS-->
