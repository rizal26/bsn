<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->
<head>
	<?php include 'header.php'; ?>
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body
	class="bg-image vertical-layout vertical-menu-modern 1-column navbar-floating footer-static menu-collapsed blank-page blank-page"
	data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
	<!-- BEGIN: Content-->
	<div class="app-content content">
		<!-- <div style="position: relative"> -->
			<?php 
			$page = (isset($_GET['page']))? $_GET['page'] : null;
				if ($page == 'register1'  || $page == 'register2' || $page == 'register3') { ?>
				<table id="logo-top" class="logobody" style="position: absolute; margin: 5px 0 0px 5px;">
					<tr>
						<td style="text-align: right;">
							<img src="../assets/images/kemendagri.png" alt="" width="70"> 
						</td>
						<td>
							<img src="../assets/images/bsn-logo.png" alt="" width="80">
						</td>
					</tr>
				</table	>
			<?php	}
			?>
			<div class="content-overlay"></div>
			<div class="header-navbar-shadow"></div>
			<div class="content-wrapper">
				<div class="content-header row">
				</div>
				<div class="content-body">
					<section class="row flexbox-container">
						<div class="col-xl-8 col-11 d-flex justify-content-center">
							<?php include 'redirect.php'; ?>
						</div>
					</section>
				</div>
			</div>
		<!-- </div> -->
	</div>
	<!-- END: Content-->
	<?php 
	if ($page == 'register1' || $page == 'register2' || $page == 'register3') { ?>
	<div style="position: relative" id="logo-bottom">
		<table class="logobody" style="position: absolute; bottom: 0; right: 0;">
			<tr>
				<td>Powered By</td>
				<td class="text-right"><img src="../assets/images/google-logo.png" alt="" width="60"></td>
				<td><img src="../assets/images/elitery-logo.jpg" width="45" alt=""></td>
			</tr>
		</table>
	</div>
	<?php } ?>
	<?php include 'js.php'; ?>
</body>
<!-- END: Body-->

</html>